// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';
import nodemailer from 'nodemailer';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default (options = {}): Hook => {
  return async (context: HookContext): Promise<HookContext> => {
const { result } = context;

     // PUT YOUR SAFE EMAIL RECIPIENTS HERE
    const safeRecipients = ["amoin@langara.ca", "amirmoin10@gmail.com", "thisistest@gmail.com", "bkoehler@langara.ca"];

    if ( !safeRecipients.includes(result['to'])) {
      console.log( "BAD: " + result['to']);
      return context;
    }

    let transporter = nodemailer.createTransport({
      host: "smtp.sendgrid.net",
      port: 587,
      secure: false,
      requireTLS: true,
      auth: {
        user: "apikey",  // SendGrid user
        pass: process.env.SENDGRID_PASSWORD // SendGrid password
      }
    });


let info = await transporter.sendMail({
      from: '"No Reply" <noreply@494914.xyz>', // PUT YOUR DOMAIN HERE
      to: result.to, // list of receivers
      subject: "Hello " + result.id, // Subject line
      text: "Your id is: " + result.id // plain text body
    });

    console.log("Message sent: %s", info.messageId);
    console.log( "GOOD: " + result['to']);
    return context;
  };
};
